# Thousand eyes exercice.

## Environment
- node 8.5.0
- npm 5.3.0

## Launch
- run `npm install`
- run `node server.js`
- Go to http://localhost:8080

## Dev
Use angular cli, if you don't have it locally use:
`npm install -g angular-cli`

Then `ng-serve` and go to http://localhost:4200
