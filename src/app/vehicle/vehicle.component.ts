import {Component, ElementRef, Input, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import {geoMercator, geoPath, scaleLinear, select} from 'd3';
import {IState} from '../interfaces/state.intf';
import {getVehiclesForRoute} from '../states/vehicle-locations/vehicle-locations.select';
import {IVehicleLocation} from '../interfaces/state.intf';

@Component({
  selector: '[tesf-vehicle]',
  template: '',
})
export class VehicleComponent implements OnInit {
  @Input() projection;

  constructor(private element: ElementRef, private store: Store<IState>) { }

  ngOnInit() {
    this.store
      .select(getVehiclesForRoute)
      .subscribe(vehicles => {
        // Join
        const elems = select(this.element.nativeElement)
          .selectAll('circle')
          .data(vehicles, v => (<any>v).id);

        // Exit
        elems.exit().remove();

        // Update
        elems
          .transition()
          .duration(5000)
          .attr('cx', v => this.projection([v.lon, v.lat])[0])
          .attr('cy', v => this.projection([v.lon, v.lat])[1]);

        // Enter
        elems.enter()
          .append('circle')
          .attr('fill', 'blue')
          .attr('r', '3px')
          .attr('cx', v => this.projection([v.lon, v.lat])[0])
          .attr('cy', v => this.projection([v.lon, v.lat])[1]);
      });

  }
}
