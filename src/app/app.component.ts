import {Observable} from 'rxjs/Observable';
import {Component, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import {identity} from 'ramda';
import 'rxjs/add/operator/filter';
import {IState} from './interfaces/state.intf';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  mapVisible$: Observable<boolean>;
  constructor(private store: Store<IState>) {}

  ngOnInit() {
    this.mapVisible$ = this.store.map(s => s.selectedRoute !== null);

    // Scroll to map on rendering
    this.mapVisible$.filter(identity).subscribe(() => {
      setTimeout(() => {
        document.querySelector('svg').scrollIntoView();
      }, 0);
    });
  }
}
