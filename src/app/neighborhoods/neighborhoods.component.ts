import {Component, OnInit, Input, ElementRef} from '@angular/core';
import * as data from '../../../sfmaps/neighborhoods.json';
import {geoMercator, geoPath, scaleLinear, select} from 'd3';

@Component({
  selector: '[tesf-neighborhoods]',
  template: '',
})
export class NeighborhoodsComponent implements OnInit {
  @Input() projection;

  constructor(private element: ElementRef) { }

  ngOnInit() {
    select(this.element.nativeElement)
      .append('path')
      .datum(<any>data)
      .attr('fill', '#E4E4E4')
      .attr('d', geoPath(this.projection));
  }
}
