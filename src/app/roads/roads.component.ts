import {Component, OnInit, Input, ElementRef} from '@angular/core';
import {geoMercator, geoPath, scaleLinear, select} from 'd3';

@Component({
  selector: '[tesf-roads]',
  template: '',
})
export class RoadsComponent implements OnInit {
  @Input() color = '#FFFFFF';
  @Input() projection;
  @Input() roads: any;

  constructor(private element: ElementRef) { }

  ngOnInit() {
    select(this.element.nativeElement)
      .append('path')
      .datum(this.roads)
      .attr('stroke', this.color)
      .attr('d', geoPath().projection(this.projection));
  }
}
