import {IActions, SELECT_ROUTE} from './selected-route.action';

export function reducer(state: string = null, action: IActions): string {
  switch (action.type) {
    case SELECT_ROUTE:
      return action.payload;
    default: return state;
  }
}
