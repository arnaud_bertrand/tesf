import {Action} from '@ngrx/store';

export const SELECT_ROUTE = '[SelectedRoute] Select route';

export class SelectAction implements Action {
  readonly type = SELECT_ROUTE;
  constructor(public payload: string) {}
}

export type IActions
    = SelectAction;
