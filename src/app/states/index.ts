import {ActionReducerMap} from '@ngrx/store';
import {reducer as vehicleLocationsReducer} from './vehicle-locations/vehicle-locations.reducer';
import {reducer as selectedRouteReducer} from './selected-route/selected-route.reducer';
import {IState} from '../interfaces/state.intf';

export const reducers: ActionReducerMap<IState> = {
  selectedRoute: selectedRouteReducer,
  vehicleLocations: vehicleLocationsReducer,
};
