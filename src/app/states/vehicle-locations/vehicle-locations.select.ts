import {IState} from '../../interfaces/state.intf';
import {IVehicleLocations} from '../../interfaces/state.intf';
import {VehicleLocationsHelper} from '../../helpers/vehicle-locations.helper';

export function getActiveRoutes({vehicleLocations}: IState): string[] {
  return VehicleLocationsHelper.getRouteTags(vehicleLocations);
}

export function getVehiclesForRoute({selectedRoute, vehicleLocations}: IState): IVehicleLocations {
  if (selectedRoute) {
    return VehicleLocationsHelper.getVehiclesForRoute(selectedRoute, vehicleLocations);
  }
  return vehicleLocations;
}
