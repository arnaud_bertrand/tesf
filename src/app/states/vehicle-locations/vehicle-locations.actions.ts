import {Action} from '@ngrx/store';

export const RETRIEVE_LOCATIONS = '[VehicleLocations] Retrieve locations';
export const RECEIVE_LOCATIONS = '[VehicleLocations] Receive locations';

export class RetrieveLocationsAction implements Action {
  readonly type = RETRIEVE_LOCATIONS;
}

export class ReceiveLocationsAction implements Action {
  readonly type = RECEIVE_LOCATIONS;
  constructor(public payload: any) {}
}

export type IActions
    = RetrieveLocationsAction
    | ReceiveLocationsAction;
