import {ReceiveLocationsAction, RETRIEVE_LOCATIONS, RetrieveLocationsAction} from './vehicle-locations.actions';
import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {Store} from '@ngrx/store';
import {Actions, Effect, toPayload} from '@ngrx/effects';
import {compose, evolve, map, prop} from 'ramda';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/delay';

@Injectable()
export class VehicleLocationsEffects {
  t = 0;
  parseNumbers = evolve({lat: l => Number(l), lon: l => Number(l)});

  @Effect() sendLocationRequest$ =
    this.actions$.ofType(RETRIEVE_LOCATIONS)
        .switchMap(() => {
          const url = `http://webservices.nextbus.com/service/publicJSONFeed?command=vehicleLocations&a=sf-muni&t=${this.t}`;
          return this.http
            .get(url)
            .map(r => this.t = r.json())
            .do((r: any) => this.t = r.lastTime.time)
        })
        .map(compose(map(this.parseNumbers), prop('vehicle')))
        .map(v => new ReceiveLocationsAction(v));

  @Effect() updateAfter15s$ =
      this.actions$.ofType(RETRIEVE_LOCATIONS)
          .delay(15000)
          .map(() => new RetrieveLocationsAction());

  constructor(private actions$: Actions, private http: Http) {}
}
