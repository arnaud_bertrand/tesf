import {IVehicleLocations} from '../../interfaces/state.intf';
import {IActions, RECEIVE_LOCATIONS} from './vehicle-locations.actions';
import {VehicleLocationsHelper} from '../../helpers/vehicle-locations.helper';

export function reducer(state: IVehicleLocations = [], action: IActions): IVehicleLocations {
  switch (action.type) {
    case RECEIVE_LOCATIONS:
      return VehicleLocationsHelper.updateVehicles(action.payload || [], state);
    default: return state;
  }
}
