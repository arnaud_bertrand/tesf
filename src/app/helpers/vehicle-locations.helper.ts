import {IVehicleLocation, IVehicleLocations} from './../interfaces/state.intf';
import {compose, equals, pluck, prop, unionWith, uniq, useWith} from 'ramda';

/** Get all uniques tags for the current list of vehicles */
const getRouteTags: (vehicles: IVehicleLocations) => string[] =
    compose(uniq, pluck('routeTag'));

/** Get vehicles for a given route */
function getVehiclesForRoute(routeTag: string, vehicles: IVehicleLocations): IVehicleLocations {
  return vehicles.filter(v => v.routeTag === routeTag);
}

/** Check if 2 vehicle are identical based on id */
const isSameId: (a: IVehicleLocation, b: IVehicleLocation) => boolean =
    useWith(equals, [prop('id'), prop('id')]);

/** Merge left for 2 arrays of vehicles */
const updateVehicles: (a: IVehicleLocations, b: IVehicleLocations) => IVehicleLocations =
    unionWith(isSameId);

export const VehicleLocationsHelper = {
  getVehiclesForRoute,
  getRouteTags,
  isSameId,
  updateVehicles,
};
