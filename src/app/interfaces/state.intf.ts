export interface IState {
  selectedRoute: string;
  vehicleLocations: IVehicleLocations;
}

export type IVehicleLocations = IVehicleLocation [];

export interface IVehicleLocation {
  id: string;
  lon: number;
  routeTag: string;
  predictable: boolean;
  speedKmHr: number;
  dirTag: string;
  heading: number;
  lat: number;
  secsSinceReport: number;
}
