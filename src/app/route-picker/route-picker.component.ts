import {Component, OnInit} from '@angular/core';
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/startWith';
import {combineLatest} from 'rxjs/observable/combineLatest';
import {Store} from '@ngrx/store';
import {IState} from '../interfaces/state.intf';
import {getActiveRoutes} from '../states/vehicle-locations/vehicle-locations.select';
import {SelectAction} from '../states/selected-route/selected-route.action';

@Component({
  selector: 'tesf-route-picker',
  templateUrl: './route-picker.component.html',
  styleUrls: ['./route-picker.component.css']
})
export class RoutePickerComponent implements OnInit {
  route = null;
  myControl: FormControl = new FormControl();

  filteredOptions: Observable<string[]>;

  constructor(private store: Store<IState>) {
  }

  ngOnInit() {
    // Update options with input
    this.filteredOptions = combineLatest(
      this.store.select(getActiveRoutes),
      this.myControl.valueChanges
        .startWith(null),
      (routes, v) => v ? routes.filter(o => o.toLowerCase().indexOf(v.toLowerCase()) > -1) : routes,
    );
  }

  selectRoute(route: any) {
    this.store.dispatch(new SelectAction(route.option.value));
  }
}
