import {drag, geoMercator, select, zoom, event, ZoomTransform} from 'd3';
import {Component, ElementRef, OnInit} from '@angular/core';
import * as arteries from '../../../sfmaps/arteries.json';
import * as freeways from '../../../sfmaps/freeways.json';
import * as streets from '../../../sfmaps/streets.json';

@Component({
  selector: '[tesf-map]',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit {
  private w = 500;
  private h = 500;
  public transform: ZoomTransform;
  public arteries: any = arteries;
  public freeways: any = freeways;
  public streets: any = streets;

  projection = geoMercator()
    .scale(160000)
    .center([-122.44, 37.77])
    .translate([this.w / 2, this.h / 2]);

  constructor(private element: ElementRef) {
  }

  ngOnInit() {
    select(this.element.nativeElement)
      .call(
        zoom()
          .scaleExtent([1, 10])
          .on('zoom', () => this.transform = event.transform)
      );

    select(this.element.nativeElement)
      .call(drag());
  }

  getTransform(t: ZoomTransform): string {
    if (!t) {
      return '';
    }
    return `translate(${t.x}, ${t.y}) scale(${t.k})`;
  }
}
