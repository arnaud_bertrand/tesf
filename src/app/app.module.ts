import {RetrieveLocationsAction} from './states/vehicle-locations/vehicle-locations.actions';
import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {HttpModule} from '@angular/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {EffectsModule} from '@ngrx/effects';
import {Store, StoreModule} from '@ngrx/store';
import {StoreDevtoolsModule} from '@ngrx/store-devtools';

import {AppComponent} from './app.component';
import {MapComponent} from './map/map.component';
import {NeighborhoodsComponent} from './neighborhoods/neighborhoods.component';
import {RoadsComponent} from './roads/roads.component';
import {IState} from './interfaces/state.intf';
import {reducers} from './states/index';
import {VehicleLocationsEffects} from './states/vehicle-locations/vehicle-locations.effects';
import {VehicleComponent} from './vehicle/vehicle.component';
import {MdInputModule, MdFormFieldModule, MdAutocompleteModule, MdOptionModule, MdCardModule, MdButtonModule} from '@angular/material';
import {RoutePickerComponent} from './route-picker/route-picker.component';

@NgModule({
  declarations: [
    AppComponent,
    MapComponent,
    NeighborhoodsComponent,
    RoadsComponent,
    VehicleComponent,
    RoutePickerComponent
  ],
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    CommonModule,
    EffectsModule.forRoot([
      VehicleLocationsEffects,
    ]),
    HttpModule,
    MdAutocompleteModule,
    MdButtonModule,
    MdCardModule,
    MdFormFieldModule,
    MdInputModule,
    MdOptionModule,
    FormsModule,
    ReactiveFormsModule,
    StoreModule.forRoot(reducers),
    StoreDevtoolsModule.instrument({maxAge: 50}),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(private store: Store<IState>) {
    // Initial trigger for retrieve location
    this.store.dispatch(new RetrieveLocationsAction());
  }
 }
